package util;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.HashMap;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import model.MonitoredData;

import java.time.LocalDate;

public class Util 
{   
    public void countDistinctDays() throws IOException{
		Files.write(Paths.get("DistinctDays.txt"),
			Arrays.asList(
				String.valueOf(
					Files.lines(Paths.get("Activities.txt"))
					.map(MonitoredData::new)
					.flatMap(c->Stream.of(c.getStartDate(),c.getEndDate()))
					.distinct()
					.count()
					)
			)
		);
		
    }
    
    public void countActivityOccurences() throws IOException{
    	Files.write(Paths.get("ActivityOccurences.txt"),
			Files.lines(Paths.get("Activities.txt"))
			.map(MonitoredData::new)
			.map(MonitoredData::getActivityLabel)
			.collect(HashMap<String,Integer>::new,
					(map,str)->{
						if(!map.containsKey(str))
							map.put(str,1);
						else
							map.put(str,map.get(str)+1);
					},
					HashMap<String,Integer>::putAll
					)
			.entrySet()
			.stream()
			.map(e->e.getKey()+" "+e.getValue())
			.collect(Collectors.toList())
		);
    }
    
    public void countActivityOccurencesPerDay() throws IOException{
    	Files.write(Paths.get("ActivityOccurencesPerDay.txt"),
			Files.lines(Paths.get("Activities.txt"))
			.map(MonitoredData::new)
			.map(MonitoredData::getStartDate)
			.distinct()
			.collect(HashMap<LocalDate,HashMap<String,Integer>>::new,
				(e, f)->
					{
						HashMap<String,Integer> newMap=null;
						try {
							newMap=Files.lines(Paths.get("Activities.txt"))
							.map(MonitoredData::new)
							.filter(s->s.getStartDate().equals(f))
							.map(MonitoredData::getActivityLabel)
							.collect(HashMap<String,Integer>::new,
									(map,str)->{
										if(!map.containsKey(str))
											map.put(str,1);
										else
											map.put(str,map.get(str)+1);
										},
									HashMap<String,Integer>::putAll
									);
						} catch (IOException e1) {
							e1.printStackTrace();
						}
						e.put(f, newMap);
					},
					HashMap<LocalDate,HashMap<String,Integer>>::putAll
					)
			.entrySet()
			.stream()
			.map(e->e.getKey()+" "+e.getValue())
			.collect(Collectors.toList())
			);
    }
    
    public void filterLongActivities() throws IOException{
    	Files.write(Paths.get("LongActivities.txt"),
			Files.lines(Paths.get("Activities.txt"))
			.map(MonitoredData::new)
			.collect(HashMap<String,Long>::new,
					(map,str)->{
						if(!map.containsKey(str.getActivityLabel()))
							map.put(str.getActivityLabel(),MonitoredData.getTimeInterval(str));
						else
							map.put(str.getActivityLabel(),map.get(str.getActivityLabel())+MonitoredData.getTimeInterval(str));
						},
					HashMap<String,Long>::putAll
					)
			.entrySet()
			.stream()
			.filter(s->s.getValue()>36000)
			.map(s->s.getKey()+" "+s.getValue()/3600+":"+
					((s.getValue()%3600/60<=10)?"0"+s.getValue()%3600/60:s.getValue()%3600/60)+":"
					+((s.getValue()%3600%60<=10)?"0"+s.getValue()%3600%60:s.getValue()%3600%60))
			.collect(Collectors.toList())
			);
    }
    
    public void filterShortActivities() throws IOException{
    	Files.write(Paths.get("ShortActivities.txt"),
			Files.lines(Paths.get("Activities.txt"))
			.map(MonitoredData::new)
			.collect(HashMap<String,Integer>::new,
					(map,str)->{
						if(!map.containsKey(str.getActivityLabel()))
							map.put(str.getActivityLabel(),
									(MonitoredData.getTimeInterval(str)<300?1001:1000));
						else
							map.put(str.getActivityLabel(),
									map.get(str.getActivityLabel())+(MonitoredData.getTimeInterval(str)<300?1001:1000));
					},
					HashMap<String,Integer>::putAll
					)
			.entrySet()
			.stream()
			.filter(s->s.getValue()/1000*((double)9/10)<=(double)s.getValue()%1000)
			.map(s->s.getKey())
			.collect(Collectors.toList())
			);
    }
}
