package model;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.Arrays;

public class MonitoredData {
	private String activityLabel;
	private LocalDate startDate;
	private LocalTime startTime;
	private LocalDate endDate;
	private LocalTime endTime;
	
	public MonitoredData(String data){
		String[] strings=data.split("\\s+");
		(Arrays.asList(strings))
		.forEach((String x)->x.trim());
		activityLabel=strings[4];
		startDate=LocalDate.parse(strings[0]);
		startTime=LocalTime.parse(strings[1]);
		endDate=LocalDate.parse(strings[2]);
		endTime=LocalTime.parse(strings[3]);
	}

	/**
	 * @return the activityLabel
	 */
	public String getActivityLabel() {
		return activityLabel;
	}

	/**
	 * @return the startDate
	 */
	public LocalDate getStartDate() {
		return startDate;
	}

	/**
	 * @return the startTime
	 */
	public LocalTime getStartTime() {
		return startTime;
	}

	/**
	 * @return the endDate
	 */
	public LocalDate getEndDate() {
		return endDate;
	}

	/**
	 * @return the endTime
	 */
	public LocalTime getEndTime() {
		return endTime;
	}
	
	public static long getTimeInterval(MonitoredData mD){
		long l1=(mD.getStartTime().getHour()*60+mD.getStartTime().getMinute())*60+mD.getStartTime().getSecond();
		long l2=(mD.getEndTime().getHour()*60+mD.getEndTime().getMinute())*60+mD.getEndTime().getSecond();
		if(l2>l1)
			return l2-l1;
		long secondsInDay=24*60*60;
		return l2+secondsInDay-l1;
	}
}
