package main;

import java.io.IOException;

import javax.swing.JOptionPane;
import javax.swing.JPanel;

import util.Util;

public class MainClass {
	public static void main( String[] args ){
		try{
	    	Util util=new Util();
	    	util.countDistinctDays();
	    	util.countActivityOccurences();
	    	util.countActivityOccurencesPerDay();
	    	util.filterLongActivities();
	    	util.filterShortActivities();
		}
		catch(IOException e){
			JOptionPane.showMessageDialog(new JPanel(),"Error reading from file","Error",JOptionPane.ERROR_MESSAGE);
		}
    }
}
